class Cart {
	constructor() {
		this.contents = []
		this.totalAmount = 0
	}

	addToCart(prod, qty) {
		this.contents.push({product: prod, quantity: qty})
		return this
	}

	showCartContents() {
		console.log(this.contents)
		return this
	}

	updateProductQuantity(prod, updatedQty) {
		this.contents.find(content => content.product.name === prod).quantity = updatedQty

		return this
	}

	clearCartContents() {
		this.contents = []
		return this
	}

	computeTotal() {
		this.totalAmount = 0
		
		let total = 0
		this.contents.forEach(content => {
			total = total + (content.product.price * content.quantity)
		})

		this.totalAmount = total

		return this
	}
}

class Customer {
	constructor(email) {
		this.email = email
		this.cart = new Cart()
		this.orders = []
	}

	checkout() {
		if(this.cart !== undefined) {
			this.orders.push({products: this.cart.contents, totalAmount: this.cart.computeTotal().totalAmount})
		}
		return this
	}
}

const john = new Customer('john@mail.com')
console.log(john)



class Product {
	constructor(name, price) {
		this.name = name
		this.price = price
		this.isActive = true
	}

	archive() {
		if(this.isActive) {
			this.isActive = false
		} else {
			console.log('Product is already inactive')
		}

		return this
	}

	updatePrice(updatedPrice) {
		this.price = updatedPrice
		return this
	}
}

const prodA = new Product('soap', 9.99)
console.log(prodA)